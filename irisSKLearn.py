# Este programa entrena una red neuronal de 3 capas para MNIST y guarda el modelo entrenado para posterior uso.

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelBinarizer
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
import numpy as np
import time
import pickle

#
from sklearn.utils import shuffle

def reader(fileName ):
    file = open(fileName, "r")
    matriz = []
    line_counter = 0
    for line in file:
        linea_matriz = line.split(",")
        matriz.append([])
        #print ("linea_matriz ",line_counter,": ",linea_matriz)
        for value in range(0, 5):
            if(value < 4):
                #print (float(linea_matriz[value]))
                matriz[line_counter].append(float(linea_matriz[value]))
                #print("contenido de la linea en la matriz", matriz[line_counter])
            else:
                #matriz[line_counter].append(linea_matriz[value][:-1])
                if(linea_matriz[value][:-1]=="Iris-setosa"):
                    matriz[line_counter].append(0)
                if(linea_matriz[value][:-1]=="Iris-versicolor"):
                    matriz[line_counter].append(2)
                if(linea_matriz[value][:-1]=="Iris-virginica"):
                    matriz[line_counter].append(3)
        line_counter += 1
       # print ("line_counter: ",line_counter,"value: ",value)
    return matriz


if __name__ == '__main__':

    # leyendo la base de datos MNIST
    data = np.asarray(shuffle(reader('iris.csv')))
    print("Lectura de la base de datos completa")
    ncol = data.shape[1] - 1
    # definiendo entradas y salidas
    n=int((60/100)*data.shape[0])
    X = data[:n,:ncol]
    y = data[:n,ncol]

    tic = time.clock()
    clf = MLPClassifier(solver='adam', alpha=1e-5, max_iter=300, activation='logistic',hidden_layer_sizes=(25,4), verbose=True, random_state=1)
    clf.fit(X, np.transpose(y))
    toc = time.clock()
    print("Entrenamiento completo")
    print("Tiempo de procesador para el entrenamiento (seg):")
    print(toc - tic)

    # definiendo valores de prueba
    #data = np.loadtxt('mnist_test.csv', delimiter=',')
    #print(data)
    #ncol = data.shape[1]
    # definiendo entradas y salidas
    X_test = data[n:,:ncol]
    y_test = data[n:,ncol]

    # predecir los valores de X_test
    predicted = clf.predict(X_test)

    # para finalizar se calcula el error
    error = 1 - accuracy_score(y_test, predicted)
    print("Error en el conjunto de prueba:")
    print(error)

    # el modelo entrenado se salva en disco
    filename = 'iris.sav'
    pickle.dump(clf, open(filename, 'wb'))